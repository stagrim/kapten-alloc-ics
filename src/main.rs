extern crate ics;

use kapten_alloc_ics::Calendar;
use std::{env, fs::{self, File}, process::exit};

fn print_help() {
    println!("ANVÄNDNING:");
    println!("  kapten-alloc-ics [FLAGGOR]\n");
    println!("FLAGGOR:");
    println!("  -a <minutes>    Sätt en påminnelse givet antal minuter innan");
    println!("  -k              Använd akademisk kvart (kalender-händelserna börjar kvart över)");
    println!("  --help, -h      Visa den här hjälpen");
    exit(0);
}

fn main() {
    let (mut alert, mut akademisk_kvart): (Option<u32>, bool) = (None, false);
    let mut args = env::args().into_iter().peekable();
    // Skip program name
    args.next();
    
    //TODO: Take input file
    while args.peek().is_some() {
        let next = args.next().unwrap();
        if next == "--help" || next == "-h" {
            println!("Kapten Alloc ICS 1.2");
            println!("Generera en ICS fil från Kapten Alloc tider\n");
            print_help();
        }
        else if next == "-a" || next == "--alert" {
            if args.peek().is_some() {
                alert = Some(args
                    .next()
                    .expect("-a flaggan tar värdet <minuter>")
                    .parse::<u32>()
                    .expect("minuter måste vara angivet med siffror")
                );
            }
            else {
                println!("Fel: inget argument givet för flaggan -a\n");
                print_help();
            }
        }
        else if next == "-k" || next == "--kvart" {
            akademisk_kvart = true;
        }
        else {
            println!("Fel: känner inte igen flaggan '{}'\n", next);
            print_help();
        }
    }

    let file = "tider.txt";
    
    let times: Vec<String> = match fs::read_to_string(file) {
        Ok(t) => 
            t.trim()
                .split("\n")
                .map(|s| s.to_string())
                .collect::<Vec<String>>(),
        Err(_) => {
            File::create(file)
                .expect("Kunde inte skapa fil");
            println!("Filen {} har skapats. Kopiera information from Kapten Alloc och klistra in i filen likt följande:\n", file);
            println!("pgk |1970-01-01|tor|00:15|Resurstid|X1.11    |Gnu      |XXx       ");
            println!("pgk |2010-07-07|ons|15:15|Labb     |X1.42    |Krabba   |XXx");
            exit(0);
        }
    };

    let mut calendar = Calendar::new(String::from("Europe/Stockholm"));

    for mut time in times {
        // Remove is_whitespace
        time.retain(|c| !c.is_whitespace());
        let cells: Vec<_> = time
            .split('|')
            .collect();

        if cells.len() < 6 {
            println!("Fel: tider.txt verkar inte vara korrekt. Studera exemplet igen");
            exit(1);
        }

        calendar.add_session(
            cells[0].to_string(),
            cells[1].replace("-", "").to_string(),
            cells[3].replace(":", "").parse::<u32>().unwrap(),
            cells[4].to_string(),
            cells[5].to_string(),
            cells[6].to_string(),
            akademisk_kvart,
            alert
        );
    }
    calendar.create_ics();

}
