extern crate ics;
extern crate rand;
extern crate chrono;

use ics::{Alarm, Daylight, Event, ICalendar, Standard};
use ics::parameters::TzIDParam;
use ics::properties::{Categories, Description, DtEnd, DtStart, LastModified, Location, RRule, Summary, Trigger, TzID, TzName};
use rand::{Rng, distributions::Alphanumeric};
use chrono::prelude::*;

// Does this make sense?
// no
// Do I want to do it
// yes
#[derive(Debug)]
struct Session<> {
    uid: String,
    dtstamp: String,
    start_time: String,
    end_time: String,
    summary: String,
    description: String,
    location: String,
}

#[derive(Debug)]
pub struct Calendar<'a> {
    calendar: ICalendar<'a>,
    time_zone: String
}

impl<'a> Calendar<'a> {
    pub fn new(time_zone: String) -> Calendar<'a> {
        let mut calendar = Calendar {
            calendar: ICalendar::new("2.0", "-//Kapten Alloc//Knepig Schemaläggning//EN"),
            time_zone
        };

        // Add time zone information
        let mut time_zone_ics = ics::TimeZone::standard(
            calendar.time_zone.to_string(),
                {
                    let mut standard = Standard::new(
                    "19810329T020000", 
                    "+0200", 
                    "+0100"
                    );
                    standard.push(RRule::new("FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU"));
                    standard.push(TzName::new("CET"));
                    standard
                }
            );
            time_zone_ics.add_daylight({
                let mut daylight = Daylight::new(
                    "19810329T020000", 
                    "+0100", 
                    "+0200"
                );
                daylight.push(RRule::new("FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU"));
                daylight.push(TzName::new("CEST"));
                daylight
            });
            calendar.calendar.add_timezone(time_zone_ics);

            calendar
    }
    
    pub fn add_session(
        &mut self, 
        course: String,
        date: String, 
        time: u32,
        r#type: String,
        group: String,
        room: String,
        akademisk_kvart: bool,
        alert: Option<u32>
    ) {
        let dtstamp: (String, u32) = (
            format!("{}", Local::now()
                .to_string()
                .replace("-", "")
                .split(" ")
                .take(1)
                .collect::<String>()
            ),
            *Local::now()
            .time()
            .to_string()
            .replace(":", "")
            .split(".")
            .map(|f| f.parse::<u32>().unwrap())
            .collect::<Vec<u32>>()
            .first().expect("Somethings wrong, I can feel it"));
        let uid: String = rand::thread_rng()
            .sample_iter(&Alphanumeric)
            .take(42)
            .map(char::from)
            .collect();
        let (start_time, end_time): (u32, u32);
        let ambulans = room.contains("Ambulans");
        
        if akademisk_kvart {
            start_time = time;
        } else {
            start_time = time - 15;
        }
        end_time = time + 185;

        let session = Session {
            uid,
            // 'Z' at the end forces UTC use
            dtstamp: format!("{}T{:06}Z", dtstamp.0, dtstamp.1),
            start_time: format!("{}T{:04}00", date, start_time),
            end_time: format!("{}T{:04}00", date, end_time),
            summary: format!("({}) {} {preposition} {}", course, r#type, room, preposition={ if ambulans { "som" } else { "i" } }),
            description: format!("Kurs: {}\\nGrupp: {}\\n{location}: {}", course, group, room, location={ if ambulans { "Roll" } else { "Rum" } }),
            location: format!("{}\\, Ole Römers väg 3\\, 223 63 Lund", room),
        };
        let mut event = Event::new(session.uid, session.dtstamp.to_string());
        event.push({
            let mut dtstart = DtStart::new(session.start_time);
            // By not ending time with a 'Z' the timezone is specified by using TZID
            dtstart.add(TzIDParam::new(self.time_zone.to_string()));
            dtstart
        });
        event.push(TzID::new(self.time_zone.to_string()));
        event.push({
            let mut dtend = DtEnd::new(session.end_time);
            dtend.add(TzIDParam::new(self.time_zone.to_string()));
            dtend
        });
        event.push(Summary::new(session.summary));
        event.push(Description::new(session.description));
        event.push(Location::new(session.location));
        event.push(LastModified::new(session.dtstamp));
        event.push(Categories::new("Handledare"));

        if alert.is_some() {
            let alarm = Alarm::audio(Trigger::new(format!("-PT{}M", alert.unwrap())));
            event.add_alarm(alarm);
        }
        
        self.calendar.add_event(event);
    }

    pub fn create_ics(&mut self) {
        //TODO: choose output file
        self.calendar
            .save_file("handledare.ics")
            .expect("Kunde inte skapa filen");
        println!("Filen handledare.ics skapad!");
    }
}
