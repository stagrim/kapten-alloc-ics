# kapten-alloc-ics

Kapten Alloc ICS är en terminal-app som genererar en ICS-fil från information tagen från [https://fileadmin.cs.lth.se/pgk/kaptenalloc/](https://fileadmin.cs.lth.se/pgk/kaptenalloc/). Kompilerade filer för Linux och Windows finns under [releases](https://gitlab.com/stagrim/kapten-alloc-ics/-/releases).

## Vad är en ICS fil?

En ICS fil är text-fil som innehåller information om kalender-händelser, så som början, slut, beskrivning, plats, påminnelser osv. Med hjälp av en ICS fil kan dessa kalender-händelser importeras till en kalender, som exempelvis den som finns till mobilen. 

## Använda programmet

```
Kapten Alloc ICS 1.2
Generera en ICS fil från Kapten Alloc tider

ANVÄNDNING:
  kapten-alloc-ics [FLAGGOR]

FLAGGOR:
  -a <minutes>    Sätt en påminnelse givet antal minuter innan
  -k              Använd akademisk kvart (kalender-händelserna börjar kvart över)
  --help, -h      Visa den här hjälpen
```

Kör programmet en gång utan några argument. Programmet kommer då att skapa en tom text fil vid namn tider.txt samt ge följande instruktioner:

```
Filen tider.txt har skapats. Kopiera information from Kapten Alloc och klistra in i filen likt följande:

pgk |1970-01-01|tor|00:15|Resurstid|X1.11    |Gnu      |XXx       
pgk |2010-07-07|ons|15:15|Labb     |X1.42    |Krabba   |XXx
```

Följ instruktionerna och klistra in tiderna från [https://fileadmin.cs.lth.se/pgk/kaptenalloc/](https://fileadmin.cs.lth.se/pgk/kaptenalloc/). Kör sedan programmet igen och ge de önskade flaggorna. Efter det så bör filen handledare.ics skapas.

## Extra information rörande att importera ICS-filen

### IOS

ICS filen måste öppnas i Safari (av någon anledning), så skicka exempelvis den genererade filen till er mail och logga in i er web mail i safari, för att sedan kunna importera den. Efter det ska importeringen vara enkel.

### Övriga enheter

Så länge inga problem har beskrivits ovan bör det fungera

## Bygga projektet

```bash
# Klona repot
$ git clone https://gitlab.com/stagrim/kapten-alloc-ics.git

# Gå till mappen
$ cd kapten-alloc-ics

# Bygg projektet (--release flaggan innebär att programmet kompileras med alla optimeringar)
$ cargo build --release

# Kör programmet (att ge argument till programmet kan bli fel, kör gärna direkt på den exekverbara filen istället)
$ cargo run

# Den exekverbara filen kommer att ligga i target/<profil>/kapten-alloc-ics
$ ./target/release/kapten-alloc-ics --help
```

