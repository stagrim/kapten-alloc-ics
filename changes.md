# Version 1.3

## Added

 - Changed description and preposition in summary for ambulance sessions

## Fixed

 - Separated lib from bin
